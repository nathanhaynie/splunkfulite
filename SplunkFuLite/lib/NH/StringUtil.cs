using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace NH
{
    public class StringUtil
    {
        private const string ERROR_INVALID_VALUE_TEXT = "Invalid value. Please try again.";

        public static string EncodeJson(string input)
        {
            string output = input;
            if (output != null)
            {
                output = output.Replace("\r\n", @"<br>").Replace("\n\r", @"<br>").Replace("\r", @"<br>").Replace("\n", @"<br>");
                output = output.Replace(@"\", @"\\");
                output = output.Replace("\"", "\\\"");
                output = output.Replace("\t", "  ");
            }
            return output;
        }
        
        public static string GetTimeFlattened()
        {
            return GetTimeFlattened(null);
        }
        public static string GetTimeFlattened(string delimiter)
        {
            return GetTimeFlattened(DateTime.Now, delimiter);
        }
        /// <summary>
        /// yyyymmddhhmmss
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static string GetTimeFlattened(DateTime datetime, string delimiter)
        {
            if (delimiter == null)
            {
                delimiter = "";
            }
            
            string output = datetime.Year.ToString() + delimiter +
                datetime.Month.ToString().PadLeft(2, '0') + delimiter +
                datetime.Day.ToString().PadLeft(2, '0') + delimiter +
                datetime.Hour.ToString().PadLeft(2, '0') + delimiter +
                datetime.Minute.ToString().PadLeft(2, '0') + delimiter +
                datetime.Second.ToString().PadLeft(2, '0');
            return output;
        }

        public static bool IsCapital(string value)
        {
            bool output = false;

            if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ".Contains(value))
            {
                output = true;
            }

            return output;
        }
        
        public static string GetRelativePathFromPhysical(string physicalPath)
        {
            return physicalPath.Replace(System.Web.HttpContext.Current.Server.MapPath("/"), "/").Replace(@"\", "/");

        }
        /// <summary>
        /// Convenience method for presentation.  Sometimes show or 
        /// hide based on a dynamic boolean value passed in;
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="show"></param>
        /// <returns></returns>
        public static string ShowOrHide(string text, bool show)
        {
            string rValue = string.Empty;
            if (show)
            {
                rValue = text;
            }
            return rValue;
        }


        public static string FolderPathRelativeToPhysical(string relativePath)
        {
            return System.Web.HttpContext.Current.Server.MapPath("/") + relativePath.Replace("/", @"\").TrimStart('\\');
        }

        public static string GetFolderNameFromRelativePath(string path)
        {
            return path.Substring(path.LastIndexOf("/") + 1);
        }

        /// <summary>
        /// for web/relative or physical paths
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFolderNameFromPath(string path)
        {
            // change all slashes to forward slashes in case of physical paths
            string tmpPath = path.Replace(@"\", "/").TrimEnd('/'); // convert them all to forward slash for convenience
            int lastpos = tmpPath.LastIndexOf("/");
            return tmpPath.Substring(lastpos + 1);
        }

        //public static string GetFileNameFromRelativePath(string path)
        //{
        //  return path.Substring(path.LastIndexOf("/") + 1);
        //}

        /// <summary>
        /// Can get file name from relative or physical paths.  Converts backslashes to forward then uses forward to parse string.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFileNameFromPath(string path)
        {
            path = path.Replace(@"\", "/");
            path = path.Substring(path.LastIndexOf("/") + 1);
            return path;


            //return path.Replace(@"\","/").Substring(path.LastIndexOf("/") + 1);
        }

        public static string GetFileExt(string path)
        {
            return path.Substring(path.LastIndexOf(".") + 1);
        }

        /// <summary>
        /// supports web or physical paths
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetBaseDirectoryFromPath(string path)
        {
            string tmpPath = path.Replace(@"\", "/"); // convert them all to forward slash for convenience
            int lastpos = tmpPath.LastIndexOf("/");
            return path.Substring(0, lastpos);
        }

        public static int ConvertToInt32(string val, int defaultValue)
        {
            int rValue = defaultValue;
            if (val == null || val.Trim().Length == 0)
            {
                // leave rValue as default
            }
            else
            {
                try
                {
                    //rValue = Convert.ToInt32(val.Trim());
                    //rValue = Convert.ToInt32(val.Trim().Trim('0').Trim('.'));
                    rValue = Convert.ToInt32(Convert.ToDecimal(val.Trim()));
                }
                catch (Exception)
                {
                    // leave rValue as default
                }
            }
            return rValue;
        }
        public static int ConvertToInt32(object val, int defaultValue)
        {
            int rValue = defaultValue;
            if (val != null)
            {
                rValue = ConvertToInt32(val.ToString(), defaultValue);
            }
            return rValue;
        }



        public static string GetIntials(string val)
        {
            string output = "";

            if (val != null && val.Trim().Length > 0)
            {
                string[] words = val.Split(' ');
                for (int i = 0; i < words.Length; i++)
                {
                    string tmp = words[i].Trim();
                    if (tmp.Length > 0)
                    {
                        output += tmp.Substring(0, 1).ToUpper();
                    }
                }
            }

            return output;
        }


        /// <summary>
        /// Convenience method to convert an object to a string first by calling object.ToString()
        /// </summary>
        /// <param name="val"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool ConvertToBoolean(object val, bool defaultValue)
        {
            bool rValue = defaultValue;
            if (val != null)
            {
                rValue = ConvertToBoolean(val.ToString(), defaultValue);
            }
            return rValue;
        }

        /// <summary>
        /// Accepts a string and evaluates to return a boolean. Input is case in-sensitive.
        /// </summary>
        /// <param name="val">string [YES|Y|TRUE|T|1]=true, [NO|N|FALSE|F|0]=false</param>
        /// <param name="defaultValue">none</param>
        /// <param name="errorIfInvalid">use only to force no errors</param>
        /// <returns></returns>
        public static bool ConvertToBoolean(string val, bool defaultValue)
        {
            bool rValue = defaultValue;
            if (val == null || val.Trim().Length == 0)
            {
                // leave rValue as default
            }
            else
            {
                try
                {
                    val = val.ToUpper();
                    if (val == "1" || val == "TRUE" || val == "T" || val == "Y" || val == "YES")
                    {
                        rValue = true;
                    }
                    else if (val == "0" || val == "FALSE" || val == "F" || val == "N" || val == "NO")
                    {
                        rValue = false;
                    }
                    else
                    {
                        // leave rValue as default
                    }
                }
                catch (Exception ex)
                {
                    // leave rValue as default
                }
            }
            return rValue;
        }

        public static decimal ConvertToDecimal(object val, decimal defaultValue)
        {
            return ConvertToDecimal(StringUtil.ConvertToString(val, null), defaultValue);
        }

        public static decimal ConvertToDecimal(string val, decimal defaultValue)
        {
            decimal rValue = defaultValue;
            if (val == null || val.Trim().Length == 0)
            {
                // leave rValue as default
            }
            else
            {
                try
                {
                    rValue = Convert.ToDecimal(val.Trim());
                }
                catch (Exception)
                {
                    // leave rValue as default
                }
            }
            return rValue;
        }

        public static string ConvertToString(object val, string defaultValue)
        {
            string rValue = defaultValue;
            if (val != null)
            {
                rValue = val.ToString();
            }
            return rValue;
        }
        public static string ConvertToString(int val, string defaultValue)
        {
            string rValue = defaultValue;
            if (val != int.MinValue)
            {
                rValue = val.ToString();
            }
            return rValue;
        }
        public static string ConvertToString(string val, string defaultValue)
        {
            string rValue = defaultValue;
            if (val != null)
            {
                rValue = val.ToString();
            }
            return rValue;
        }

        public static string ConvertToYesNo(bool val, bool capitalizeFirstLetter)
        {
            string rValue = string.Empty;
            if (capitalizeFirstLetter)
            {
                rValue = ConvertToYesNo(val, "Yes", "No");
            }
            else
            {
                rValue = ConvertToYesNo(val, "yes", "no");
            }
            return rValue;
        }

        public static string ConvertToYesNo(bool val, string yesValue, string noValue)
        {
            string rValue = string.Empty;
            if (val)
            {
                rValue = yesValue;
            }
            else
            {
                rValue = noValue;
            }

            return rValue;
        }

        /// <summary>
        /// If value is null, then returns the replaceValue instead.  Otherwise
        /// returns the value itself unchanged.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="replaceValue"></param>
        /// <returns>string</returns>
        public static string ReplaceNull(string val, string replaceValue)
        {
            if (val == null)
            {
                return replaceValue;
            }
            else
            {
                return val;
            }
        }

        public static string ReplaceNullOrZeroLength(string val, string replaceValue)
        {
            if (val == null || val.Length == 0)
            {
                return replaceValue;
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// If no replacement parameter provided, returns zero-length string by default.
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string ReplaceNull(string val)
        {
            if (val == null)
            {
                return string.Empty;
            }
            else
            {
                return val;
            }
        }

        public static string ReplaceSpace()
        {
            return ReplaceSpace("");
        }

        public static string ReplaceSpace(string val)
        {
            if (val == null)
            {
                return string.Empty;
            }
            else
            {
                val = val.Replace(" ", val);
                return val;
            }
        }

        /// <summary>
        /// The value passed in will be trimmed and checked for zero length.
        /// If zero-length, replacement value will be returned otherwise
        /// the unchanged value will be returned.
        /// </summary>
        /// <param name="val"></param>
        /// <returns>string</returns>
        public static string ReplaceNullTrimZero(string val, string replaceValue)
        {
            if (val != null && val.Trim().Length > 0)
            {
                return val;
            }
            else
            {
                return replaceValue;
            }
        }
        /* consider this code...
        public static DateTime ConvertToDateTime(string val)
            {
                return DateTime.Parse(val);
            }
            */

        public static DateTime ConvertToDateTime(string val)
        {
            return ConvertToDateTime(val, DateTime.MinValue);
        }

        public static DateTime ConvertToDateTime(object val, DateTime defaultValue)
        {
            return ConvertToDateTime(val.ToString(), defaultValue);
        }
        public static DateTime ConvertToDateTime(string val, DateTime defaultValue)
        {
            DateTime output = defaultValue;
            if (val == null || val.Trim().Length == 0)
            {
                // leave rValue as default
            }
            else
            {
                try
                {
                    output = DateTime.Parse(val);
                }
                catch (Exception)
                {
                    // leave rValue as default
                }
            }
            return output;
        }

        public static string Truncate(string val, int maxLength)
        {
            return Truncate(val, maxLength, string.Empty);
        }
        public static string Truncate(string val, int maxLength, string addText)
        {
            string rValue = val;

            if (rValue.Length > maxLength)
            {
                rValue = rValue.Substring(0, maxLength - addText.Length) + addText;
            }

            return rValue;
        }

        public static string ConvertURLsToHyperlinks(string input)
        {
            return Regex.Replace(input, @"(\bhttp://[^ ]+\b)", @"<a href=""$0"">$0</a>");
        }

        public static string HighlightKeywords(string text, string keywords)
        {
            // Swap out the ,<space> for pipes and add the braces
            Regex r = new Regex(@", ?");
            keywords = "(" + r.Replace(keywords, @"|") + ")";

            // Get ready to replace the keywords
            r = new Regex(keywords, RegexOptions.Singleline | RegexOptions.IgnoreCase);

            // Do the replace
            return r.Replace(text, new MatchEvaluator(MatchEval));
        }

        public static string MatchEval(Match match)
        {
            if (match.Groups[1].Success)
            {
                //return "<b>" + match.ToString() + "</b>";
                return HighlightPrefix + match.ToString() + HighlightPostfix;
            }
            return ""; //no match
        }

        //public static string HighlightPrefix = "<b>";
        //public static string HighlightPostfix = "</b>";
        public static string HighlightPrefix = "<font class=\"HL\">";
        public static string HighlightPostfix = "</font>";

        public static bool IsNumeric(object value)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(value), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static bool IsNumeric(List<object> input_list)
        {
            return IsNumeric(input_list, true);
        }

        public static bool IsNumeric(List<object> input_list, bool ignore_blanks)
        {
            bool output = false;

            foreach (object value in input_list)
            {
                if (value == null || value.ToString().Length == 0)
                {
                    if (ignore_blanks)
                    {
                        // ignore
                    }
                    else
                    {
                        output = false;
                        break;
                    }
                }
                else
                {
                    if (IsNumeric(value))
                    {
                        output = true;
                    }
                    else
                    {
                        output = false;
                        break;
                    }
                }
            }

            return output;
        }

        public static bool IsDateTime(object expression)
        {
            bool output = false;
            DateTime date_value;
            if(DateTime.TryParse(expression.ToString(), out date_value))
            {
                output = true;
            } 
            
            return output;
        }

        public static bool IsDateTime(List<object> input_list)
        {
            return IsDateTime(input_list, true);
        }

        public static bool IsDateTime(List<object> input_list, bool ignore_blanks)
        {
            bool output = false;

            foreach (object value in input_list)
            {
                if (value == null || value.ToString().Length == 0)
                {
                    if (ignore_blanks)
                    {
                        // ignore
                    }
                    else
                    {
                        output = false;
                        break;
                    }
                }
                else
                {
                    if (IsDateTime(value))
                    {
                        output = true;
                    }
                    else
                    {
                        output = false;
                        break;
                    }
                }
            }

            return output;
        }

        public static string HtmlEncode(string input)
        {
            return System.Net.WebUtility.HtmlEncode(input);
        }

        //public static Regex CSVSplitRegex = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.None);

        public static List<string> SplitCSV(string input)
        {
            // DO NOT PARAMATERIZE THE REGEX... huge memory leak troubleshooting
            List<string> output = new List<string>();
            Regex csvSplit = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.None);

            foreach (Match match in csvSplit.Matches(input))
            {
                output.Add(match.Value.TrimStart(','));
            }
            csvSplit.Matches("");
            return output;

            //==================================================
            // ORIGINAL CODE
            //==================================================
            //List<string> output = new List<string>();
            //Regex csvSplit = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.Compiled);

            //foreach (Match match in csvSplit.Matches(input))
            //{
            //    output.Add(match.Value.TrimStart(','));
            //}
            //return output;
            //==================================================
        }

        public static List<string> SplitPSV(string input)
        {
            List<string> output = new List<string>();
            Regex csvSplit = new Regex("(?:^|\\|)(\"(?:[^\"]+|\"\")*\"|[^\\|]*)", RegexOptions.None);

            foreach (Match match in csvSplit.Matches(input))
            {
                output.Add(match.Value.TrimStart('|'));
            }
            csvSplit.Matches("");
            return output;

            //==================================================
            // ORIGINAL CODE
            //==================================================
            //List<string> output = new List<string>();
            //Regex csvSplit = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.Compiled);

            //foreach (Match match in csvSplit.Matches(input))
            //{
            //    output.Add(match.Value.TrimStart(','));
            //}
            //return output;
            //==================================================
        }

        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex, int length)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = param.Substring(startIndex);
            //return the result of the operation
            return result;
        }

        public static string GetMatch(string text, string regex) 
        {
            string output = "";

            Match match = Regex.Match(text, regex);

            if (match.Success)
            {
                output = match.Value;
            }

            return output;
        }
    }
}