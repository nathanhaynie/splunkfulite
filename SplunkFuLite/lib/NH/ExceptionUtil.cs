﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NH
{
    public static class ExceptionUtil
    {
        public static string FormatException(Exception ex)
        {
            StringBuilder msg = new StringBuilder();

            msg.Append("Data: " + ex.Data + "\n");
            msg.Append("Source: " + ex.Source + "\n");
            msg.Append("TargetSite: " + ex.TargetSite + "\n");
            msg.Append("Message: " + ex.Message + "\n");
            msg.Append("StackTrace: " + ex.StackTrace + "\n");
            if (ex.InnerException != null)
            {
                msg.Append("[Inner Exception]\n");
                msg.Append("  Data: " + ex.InnerException.Data + "\n");
                msg.Append("  Source: " + ex.InnerException.Source + "\n");
                msg.Append("  TargetSite: " + ex.InnerException.TargetSite + "\n");
                msg.Append("  Message: " + ex.InnerException.Message + "\n");
                msg.Append("  StackTrace: " + ex.InnerException.StackTrace + "\n");
            }

            return msg.ToString();
        }
    }
}
