using System;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace NH
{
    /// <summary>
    /// Summary description for IoUtil.
    /// </summary>
    public class IoUtil
    {
        public IoUtil()
        {
            //
            // Blank Constructor
            //
        }

        public static Boolean FileDelete(string path)
        {
            Boolean r = false;
            try
            {
                File.Delete(path);
                r = true;
            }
            catch (Exception ex)
            {
                // throw new ApplicationException(e.ToString());
                // TODO: log here
                r = false;
            }
            return r;
        }

        public static Boolean FolderDelete(string path)
        {
            return FolderDelete(path, false);
        }
        public static Boolean FolderDelete(string path, bool recursive)
        {
            Boolean r = false;
            try
            {
                Directory.Delete(path, recursive);
                r = true;
            }
            catch (Exception ex)
            {
                // throw new ApplicationException(e.ToString());
                // TODO: log here
                r = false;
            }
            return r;
        }

        public static void SaveFileContents(string content, string path)
        {
            FileWrite(content, path);
        }

        /// <summary>
        /// Will overwrite if already exists
        /// </summary>
        /// <param name="content"></param>
        /// <param name="path"></param>
        public static void FileWrite(string content, string path)
        {
            StreamWriter sw = new StreamWriter(path, false, Encoding.ASCII);
            sw.Write(content);
            sw.Close();
        }

        public static void FileWrite(string content, string file_path, bool create_folder)
        {
            if (create_folder)
            {
                string folder_path = IoUtil.GetDirectoryFromPath(file_path);
                IoUtil.CreateDirectory(folder_path);
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(file_path))
            {
                file.Write(content);
            }
                        
            //StreamWriter sw = new StreamWriter(path, false, Encoding.ASCII);
            //sw.Write(content);
            //sw.Close();
        }

        

        public static string GetFileContents(string path)
        {
            StreamReader sr = new StreamReader(path);
            string retValue = sr.ReadToEnd();
            sr.Close();
            return retValue;
        }


        public static bool DeleteFilesStartWith(string physDirectoryPath, string fileStartsWith, bool checkSuccess)
        {
            bool rValue = true;
            DeleteFilesStartWith(physDirectoryPath, fileStartsWith);
            if (checkSuccess)
            {
                string[] files = Directory.GetFiles(physDirectoryPath);
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = StringUtil.GetFileNameFromPath(files[i]);
                    if (fileName.StartsWith(fileStartsWith))
                    {
                        rValue = false;
                        break;
                    }

                }
            }

            return rValue;
        }

        public static void DeleteFilesStartWith(string physDirectoryPath, string fileStartsWith)
        {

            string[] files = Directory.GetFiles(physDirectoryPath);
            for (int i = 0; i < files.Length; i++)
            {
                //string file = files[i];
                string fileName = StringUtil.GetFileNameFromPath(files[i]);
                if (fileName.StartsWith(fileStartsWith))
                {
                    FileDelete(files[i]);
                }

            }

        }

        public static bool DeleteFilesEndWith(string physDirectoryPath, string fileEndWith, bool checkSuccess)
        {
            bool rValue = true;
            DeleteFilesEndWith(physDirectoryPath, fileEndWith);
            if (checkSuccess)
            {
                string[] files = Directory.GetFiles(physDirectoryPath);
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = StringUtil.GetFileNameFromPath(files[i]);
                    if (fileName.EndsWith(fileEndWith))
                    {
                        rValue = false;
                        break;
                    }

                }
            }

            return rValue;
        }

        public static void DeleteFilesEndWith(string physDirectoryPath, string fileEndWith)
        {

            string[] files = Directory.GetFiles(physDirectoryPath);
            for (int i = 0; i < files.Length; i++)
            {
                //string file = files[i];
                string fileName = StringUtil.GetFileNameFromPath(files[i]);
                if (fileName.EndsWith(fileEndWith))
                {
                    FileDelete(files[i]);
                }

            }

        }

        /// <summary>
        /// Convenience method for building directories if they do not exist
        /// </summary>
        /// <param name="path"></param>
        public static string CreateDirectory(string path)
        {
            string returnVal;
            if (!Directory.Exists(path))
            {
                // create output directory for C# business classes
                Directory.CreateDirectory(path);
                // results.Append("Directory " + path + " created successfully.\n");
                returnVal = "Directory " + path + " created successfully.\n";
            }
            else
            {
                returnVal = "Directory " + path + " already exists. No action taken.\n";
            }
            return returnVal;
        }

        public static bool DirectoryExists(string path)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool FileExists(string path)
        {
            if (File.Exists(path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool FileExists(string folder_path, string starts_with)
        {
            bool output = false;
            
            string[] files = Directory.GetFiles(folder_path);

            foreach(string file in files) 
            {
                if(file.StartsWith(folder_path + starts_with)) 
                {
                    output = true;
                }
            }
            return output;
        }

        /// <summary>
        /// If file exists, check for versions and return NEXT versioned filename
        /// </summary>
        /// <param name="file_path"></param>
        /// <returns></returns>
        public static string IncrementFileName(string file_path, string extension, int character_count)
        {
            // assumes file_path is non-versioned (no _0001.xlsx) versions included in name
            
            string output = file_path;

            if (File.Exists(file_path))
            {
                for(int i=0; i<1000; i++) 
                {
                    string version = "_" + i.ToString().PadLeft(character_count, '0');
                    
                    string next_file_path = file_path.Replace(extension, version + extension);
                    
                    if(!IoUtil.FileExists(next_file_path))
                    {
                        output = next_file_path;
                        break;
                    }
                }
            }

            return output;
        }

        /// <summary>
        /// If file exists, check for versions and return NEXT versioned filename
        /// </summary>
        /// <param name="file_path"></param>
        /// <returns></returns>
        public static string TimeStampFileName(string file_path, string extension, int character_count)
        {
            // assumes file_path is non-versioned (no _0001.xlsx) versions included in name

            string output = file_path;

            //if (File.Exists(file_path))
            //{
                string version = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
                string new_file_path = file_path.Replace(extension, "_" + version + extension);
                output = new_file_path;

                // only append number if same version suffix already exists - unlikely unless super-high frequency
                if (File.Exists(new_file_path))
                {
                    for (int i = 0; i < 1000; i++)
                    {
                        string v = "_" + i.ToString().PadLeft(character_count, '0');
                        //string v = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');

                        string next_file_path = new_file_path.Replace(extension, v + extension);

                        if (!IoUtil.FileExists(next_file_path))
                        {
                            output = next_file_path;
                            break;
                        }
                    }
                }
            //}

            return output;
        }

        public static bool FileExists(string folder_path, string starts_with, string ends_with)
        {
            bool output = false;

            string[] files = Directory.GetFiles(folder_path);

            foreach (string file in files)
            {
                if (file.StartsWith(folder_path + starts_with) && file.EndsWith(ends_with))
                {
                    output = true;
                }
            }
            return output;
        }

        public static bool FileCopy(string from, string to)
        {
            bool output = false;
            
            if (File.Exists(from))
            {
                File.Copy(from, to);
                if (!File.Exists(from) && File.Exists(to))
                {
                    output = true;
                }
            }
            else
            {
                output = false;
            }
            return output;
        }

        public static bool FileMove(string from, string to)
        {
            return FileMove(from, to, false);
        }

        public static bool FileMove(string from, string to, bool overwrite)
        {
            bool output = false;

            if (File.Exists(from))
            {
                if (File.Exists(to))
                {
                    File.Delete(to);
                }
                
                File.Move(from, to);
                if (!File.Exists(from) && File.Exists(to))
                {
                    output = true;
                }
            }
            else
            {
                output = false;
            }
            return output;
        }

        public static void FileAppend(string content, string path)
        {
            //StreamWriter sw = new StreamWriter(path, false, Encoding.ASCII);
            //sw.ap(content);
            //sw.Close();
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(content);
            }
        }

        /// <summary>
        /// Get path for a file that starts with specified string by directory path
        /// </summary>
        /// <param name="list_id"></param>
        /// <returns></returns>
        private static string GetFileStartingWith(string directory_path, string starts_with)
        {
            // TODO: this is not yet tested
            string output = "";
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(directory_path);
            foreach (System.IO.FileInfo fi in di.EnumerateFiles())
            {
                if (fi.Name.StartsWith(starts_with))
                {
                    output = fi.Directory + @"\" + fi.Name;
                }
            }
            return output;
        }

        /// <summary>
        /// Get path for a file that starts with specified string by directory path
        /// </summary>
        /// <param name="list_id"></param>
        /// <returns></returns>
        public static List<string> GetFilesStartingWith(string directory_path, string starts_with)
        {
            // TODO: this is not yet tested
            List<string> output = new List<string>();
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(directory_path);
            foreach (System.IO.FileInfo fi in di.EnumerateFiles())
            {
                if (fi.Name.StartsWith(starts_with))
                {
                    output.Add(fi.Directory + @"\" + fi.Name);
                }
            }
            return output;
        }

        public static List<string> GetSubDirectories(string directory_path)
        {
            List<string> directories = new List<string>();
            string[] dirs = Directory.GetDirectories(directory_path);
            for (int i = 0; i < dirs.Length; i++)
            {
                directories.Add(dirs[i]);
            }
            return directories;
        }

        public static List<string> GetFiles(string directory_path)
        {
            List<string> directories = new List<string>();
            string[] files = Directory.GetFiles(directory_path);
            for (int i = 0; i < files.Length; i++)
            {
                directories.Add(files[i]);
            }
            return directories;
        }

        public static List<string> GetListFromFile(string path)
        {
            List<string> output = new List<string>();

            if (FileExists(path))
            {
                string[] contents = GetFileContents(path).Split('\n');
                foreach (string line in contents)
                {
                    output.Add(line.Trim());
                }
            }

            return output;
        }

        public static string GetFileNameFromPath(string path)
        {
            int startPosition = path.LastIndexOf("\\") + 1;
            return path.Substring(startPosition, (path.Length - startPosition));
        }

        public static string GetDirectoryFromPath(string path)
        {
            int pos1 = path.LastIndexOf("\\") + 1;
            return path.Substring(0, pos1);
        }

        public static int CountFileLines(string filePath)
        {
            int count = 0;
            using (StreamReader r = new StreamReader(filePath))
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    count++;
                }
            }
            return count;
        }

        public static void OpenFile(string file_path)
        {
            //File.Open(file_path,FileMode.Open);
            System.Diagnostics.Process.Start(file_path);
        }

        public static string RemoveDoubleSlashes(string input)
        {
            return input.Replace(@"\\",@"\");
        }

        public static void ExecuteProcess(string fileName, string arguments, out string standardOutput, out string standardError)
        {
            using (var process = new Process())
            {
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.FileName = fileName;
                process.StartInfo.Arguments = arguments;

                process.Start();

                //Thread.Sleep(100);

                using (Task processWaiter = Task.Factory.StartNew(() => process.WaitForExit()))
                using (Task<string> outputReader = Task.Factory.StartNew(() => process.StandardOutput.ReadToEnd()))
                using (Task<string> errorReader = Task.Factory.StartNew(() => process.StandardError.ReadToEnd()))
                {
                    Task.WaitAll(processWaiter, outputReader, errorReader);

                    standardOutput = outputReader.Result;
                    standardError = errorReader.Result;
                }
            }
        }
    }
}
