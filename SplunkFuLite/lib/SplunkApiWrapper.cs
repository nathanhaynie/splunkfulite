﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;

namespace SplunkFuLite
{
    public static class SplunkApiWrapper
    {
        /// <summary>
        /// RunSearch
        /// </summary>
        /// <param name="earliestTime"></param>
        /// <param name="latestTime"></param>
        /// <param name="search"></param>
        /// <param name="outputType"></param>
        /// <param name="filePath"></param>
        public static int RunSearch(string server, string port, string username, string password, string app_name, DateTime earliest, DateTime latest, string search, string output_type, string filePath, int max_records)
        {
            int seconds_duration = Convert.ToInt32((latest - earliest).TotalSeconds);
            
            string earliest_time = ConvertToEpoch(earliest).ToString();
            string latest_time = ConvertToEpoch(latest).ToString();
                        
            
            //search = search.Replace("%22", "\"");
            int first_pos = server.IndexOf("//") + 2;
            int second_pos = server.IndexOf("/", first_pos + 1);
            string server_name = server.Substring(first_pos);

            search = search.Replace("[%server_name%]", server_name);
            search = search.Replace("[[server_name]]", server_name);
            filePath = filePath.Replace("[%server_name%]", server_name);
            filePath = filePath.Replace("[[server_name]]", server_name);

            Regex regex = new Regex(@"^\w+");
            Match match = regex.Match(server_name);
            if (match.Success)
            {
                server_name = match.Value;
            }
            Console.WriteLine("server_name=" + server_name);

            int recordCounter = 0;

            NetworkCredential creds = new NetworkCredential(username, password);
            
            // api documentation
            // http://dev.splunk.com/view/python-sdk/SP-CAAAEE5

            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            HttpWebRequest req = null;
            if (app_name != null)
            {
                // use namespace-aware syntax
                req = WebRequest.Create("" + server + ":" + port + "/servicesNS/" + username + "/" + app_name + "/search/jobs") as HttpWebRequest;
                //req = WebRequest.Create("" + server + ":" + port + "/servicesNS/" + username + "/" + app_name + "/jobs") as HttpWebRequest;
            }
            else
            {
                // use normal syntax (searches against /search/ app
                req = WebRequest.Create("" + server + ":" + port + "/services/search/jobs") as HttpWebRequest;
            }


            req.Credentials = creds;
            req.Method = "POST";
            //req.KeepAlive = false;
            string body = "";
            if (search.Trim().StartsWith("| tstats"))
            {
                body = ("timeout=86400&earliest_time=" + System.Web.HttpUtility.UrlEncode(earliest_time) + "&latest_time=" + System.Web.HttpUtility.UrlEncode(latest_time) + "&search=" + HttpUtility.UrlEncode(search));
            }
            else
            {
                body = ("timeout=86400&earliest_time=" + System.Web.HttpUtility.UrlEncode(earliest_time) + "&latest_time=" + System.Web.HttpUtility.UrlEncode(latest_time) + "&search=search+" + HttpUtility.UrlEncode(search));
            }

            Console.WriteLine("URI: " + req.RequestUri);
            Console.WriteLine("BODY: " + body);
            Console.WriteLine("Starting search: "); //: " + send);

            StreamWriter sw = new StreamWriter(req.GetRequestStream());
            sw.Write(body);
            sw.Close();

            HttpWebResponse res = req.GetResponse() as HttpWebResponse;
            StreamReader sr = new StreamReader(res.GetResponseStream());
            string resp = sr.ReadToEnd();
            sr.Close();

            XmlDocument xd = new XmlDocument();
            xd.LoadXml(resp);

            string sid = xd.SelectSingleNode("/response/sid").InnerText;

            bool done = false;
            bool fail = false;

            Console.Write("Checking for results: ");

            string jobs_url = server + ":" + port + "/services/search/jobs";
            if (app_name != null)
            {
                // use namespace-aware syntax
                jobs_url = server + ":" + port + "/servicesNS/" + username + "/" + app_name + "/search/jobs";
            }


            while (true)
            {
                Console.Write(".");

                Thread.Sleep(1000);
                req = WebRequest.Create(jobs_url + "/" + sid) as HttpWebRequest;
                req.Credentials = creds;

                res = req.GetResponse() as HttpWebResponse;
                sr = new StreamReader(res.GetResponseStream());
                resp = sr.ReadToEnd();
                sr.Close();

                xd.LoadXml(resp);
                XmlNamespaceManager man = new XmlNamespaceManager(xd.NameTable);
                man.AddNamespace("s", "http://dev.splunk.com/ns/rest");
                string isDone = xd.SelectSingleNode("//s:key[@name=\"isDone\"]", man).InnerText;
                string isFail = xd.SelectSingleNode("//s:key[@name=\"isFailed\"]", man).InnerText;

                if (isDone == "1")
                {
                    done = true;
                    break;
                }
                if (isFail == "1")
                {
                    fail = true;
                    break;
                }
            }

            if (fail)
            {
                // Handle fail
            }
            else if (done)
            {
                string url = req.RequestUri + "/results?";
                if (output_type == "csv")
                {
                    url += "output_mode=csv";
                }

                bool finished = false;
                int pageCounter = 0;

                Console.Write("\nDownloading results pages: ");

                while (finished == false)
                {
                    pageCounter++;

                    int downloadedRecordCount = DownloadResults(url, "csv", max_records, pageCounter, creds, filePath, earliest, seconds_duration);
                    recordCounter += downloadedRecordCount;
                    if (downloadedRecordCount < max_records)
                    {
                        finished = true;
                    }
                }
            }
            return recordCounter;
        }

        private static int DownloadResults(string baseUrl, string output_mode, int pageSize, int pageNumber, NetworkCredential creds, string filePath, DateTime start, int secondsDuration)
        {
            string url = baseUrl + "&count=" + pageSize.ToString();
            if (pageNumber > 1)
            {
                url += "&offset=" + (pageNumber - 1) * pageSize;
            }
            //?output_mode=csv&field_list=<comma separated field list>&count=0

            //req = WebRequest.Create(req.RequestUri + "/results") as HttpWebRequest;
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.Credentials = creds;

            HttpWebResponse res = req.GetResponse() as HttpWebResponse;
            StreamReader sr = new StreamReader(res.GetResponseStream());
            string resp = sr.ReadToEnd();
            sr.Close();

            string startTime = start.Year.ToString() + start.Month.ToString().PadLeft(2, '0') + start.Day.ToString().PadLeft(2, '0') + "-" + start.Hour.ToString().PadLeft(2, '0') + start.Minute.ToString().PadLeft(2, '0') + start.Second.ToString().PadLeft(2, '0');

            filePath = filePath.Replace(".csv", "_" + startTime + "_" + secondsDuration.ToString() + "_" + pageNumber.ToString().PadLeft(3, '0') + ".csv");

            System.IO.File.WriteAllText(filePath, resp);  //(@"C:\Users\Public\TestFolder\WriteLines.txt", resp);

            if (pageNumber == 1)
            {
                Console.WriteLine("\n" + filePath);
            }
            Console.Write(pageNumber + ".");

            int output = 0;

            if (System.IO.File.Exists(filePath))
            {
                int lineCount = CountFileLines(filePath);
                if (lineCount > 0)
                {
                    output = lineCount - 1; // to account for the header line
                }
            }
            return output;
        }

        private static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static long ConvertToEpoch(DateTime datetime)
        {
            return ConvertToEpoch(datetime, -7); // 7 for pst?
        }

        public static long ConvertToEpoch(DateTime datetime, int hours_offset)
        {
            //DateTime epochTime = new DateTime(1969, 12, 31, 17, 0, 0);  // use this date w/ offset to 1970 for local timezone hack
            DateTime epochTime = new DateTime(1970, 1, 1, 0, 0, 0);  // use this date w/ offset to 1970 for local timezone hack
            epochTime = epochTime.AddHours(hours_offset);
            TimeSpan t = datetime - epochTime;
            int secondsSinceEpoch = (int)t.TotalSeconds;

            return secondsSinceEpoch;
        }

        public static int CountFileLines(string filePath)
        {
            int count = 0;
            using (StreamReader r = new StreamReader(filePath))
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    count++;
                }
            }
            return count;
        }
    }
}
