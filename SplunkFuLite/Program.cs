﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using NH;

namespace NH.SplunkFu
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> errors = new List<string>();

            StringBuilder sb = new StringBuilder();

            // TODO make it so config file path can accept spaces in path like /Program Files/
            if (args.Length == 0 || args[0].ToUpper() == "HELP")
            {
                sb.AppendLine("Syntax: SplunkFu <parameters> ");
                sb.AppendLine("\tserver \t server url (example: https://mysplunkserver)");
                sb.AppendLine("\tport \t port (default: 8089)");
                sb.AppendLine("\tu \t username");
                sb.AppendLine("\tp \t password");
                sb.AppendLine("\tq \t query");
                sb.AppendLine("\ta \t splunk app name");
                sb.AppendLine("\to \t output directory path");
                sb.AppendLine("\tf \t filename (.csv optional)");
                sb.AppendLine("\ts \t start time :ISO 8601: YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00) - TZD ignored");
                sb.AppendLine("\te \t end time :ISO 8601: YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00) - TZD ignored");
                sb.AppendLine("query_file_path - instead of providing a query, provide a path to a text file with a query in it.  Helps when there are double quotes.");
                //sb.AppendLine("Only csv output mode supported at this time.");
                sb.AppendLine("May the SplunkFu be with you!");
                System.Console.WriteLine(sb);
            }
            else if (args.Length > 0)
            {
                List<string> servers = new List<string>();
                string username = null;
                string password = null;
                string query = null;
                string output_directory = null;
                string filename_template = null;
                string app_name = null;
                string server = null;
                string port = "8089";
                string directory_path = "";
                string starts_with = "";
                string output_file_path = "";
                string query_file_path = "";

                DateTime start = DateTime.MinValue;
                DateTime end = DateTime.MinValue;

                foreach (string arg in args)
                {
                    string[] s = arg.Split('=');
                    string key = s[0].ToUpper();
                    string val = null;
                    
                    if (s.Length > 1)
                    {
                        val = s[1];
                    }

                    switch (key)
                    {
                        case "SERVER":
                            server = val.Trim();
                            break;
                        case "PORT":
                            port = val;
                            break;
                        case "A":
                            app_name = val;
                            break;
                        case "U":
                            username = val;
                            break;
                        case "P":
                            password = val;
                            break;
                        case "Q":
                            query = arg.TrimStart("q=".ToCharArray()).Replace("&quot;", "\"");
                            break;
                        case "O":
                            output_directory = val;
                            break;
                        case "F":
                            filename_template = val;
                            break;
                        case "S":
                            start = ParseDateTime(val);
                            break;
                        case "E":
                            end = ParseDateTime(val);
                            break;
                        case "DIRECTORY_PATH":
                            directory_path = val;
                            break;
                        case "STARTS_WITH":
                            starts_with = val;
                            break;
                        case "OUTPUT_FILE_PATH":
                            output_file_path = val;
                            break;
                        case "QUERY_FILE_PATH":
                            query_file_path = val;
                            break;
                    }
                    if (key != "P")
                    {
                        Console.WriteLine(key + "=" + val);
                    }
                    else
                    {
                        Console.WriteLine(key + "=*****");
                    }
                }

                // load query if applicable
                if (query_file_path.Length > 0)
                {
                    if (IoUtil.FileExists(query_file_path))
                    {
                        query = IoUtil.GetFileContents(query_file_path);
                    }
                    else
                    {
                        throw new ApplicationException("required query_file_path not found: " + query_file_path);
                    }
                }




                // check required parms
                if (server == null || server.Length == 0)//server == null && server_list == null)
                {
                    //throw new ApplicationException("");
                    errors.Add("missing required parameter: server [server]");
                }
                if (port == null)
                {
                    //throw new ApplicationException("missing required parameter: port");
                    errors.Add("missing required parameter: port [port]");
                }
                if (username == null)
                {
                    //throw new ApplicationException("missing required parameter: username");
                    errors.Add("missing required parameter: username [u]");
                }
                if (password == null)
                {
                    //throw new ApplicationException("missing required parameter: password");
                    errors.Add("missing required parameter: password [p]");
                }


                if (query == null)
                {
                    //throw new ApplicationException("missing required parameter: query");
                    errors.Add("missing required parameter: query [q]");
                }
                //if (outputDirectory == null)
                //{
                //    //throw new ApplicationException("missing required parameter: outputDirectory");
                //    errors.Add("missing required parameter: outputDirectory [o]");
                //}
                if (filename_template == null)
                {
                    //throw new ApplicationException("missing required parameter: fileNameTemplate");
                    errors.Add("missing required parameter: filename_template [f]");
                }
                if (start == DateTime.MinValue)
                {
                    //throw new ApplicationException("missing required parameter: start");
                    errors.Add("missing required parameter: start [s]");
                }
                if (end == DateTime.MinValue)
                {
                    //throw new ApplicationException("missing required parameter: end");
                    errors.Add("missing required parameter: end [e]");
                }

                if (errors.Count > 0)
                {
                    StringBuilder error_msg = new StringBuilder();
                    foreach (string error in errors)
                    {
                        error_msg.AppendLine("ERROR: " + error);
                    }
                    Console.Write(error_msg);
                    Environment.Exit(0);
                }

                //SplunkFuSearchBatch batch = new SplunkFuSearchBatch();
                //batch.JobType = SplunkFuSearchBatch.JOBTYPE.Single;
                //batch.ServerUrls = servers;
                //batch.Username = username;
                //batch.Password = password;
                //batch.AppName = app_name;
                //batch.QueryTemplate = query;
                ////batch.OutputFolder = outputDirectory;
                //batch.FileName = filename_template;
                //batch.StartDateTime = start;
                //if (end > DateTime.MinValue)
                //{
                //    batch.EndDateTime = end;
                //    batch.WindowCount = 1; // only one window when specifying start and end
                //}

                //RunQuery(servers, port, username, password, app_name, query, outputDirectory, fileNameTemplate, start, end);
                //RunQuery(job);
                int error_count = 0;
                int output = 0;
                try
                {
                    //output = NH.SplunkFu.SplunkUtil.RunBatch(batch, false);
                    string file_path = output_directory + "\\" + filename_template;
                    output = SplunkFuLite.SplunkApiWrapper.RunSearch(server, port, username, password, app_name, start, end, query, "csv", file_path, 10000);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Houston! We have a problem...");
                    Console.WriteLine(StringUtil.GetTimeFlattened() + "ERROR: " + ExceptionUtil.FormatException(ex));
                    error_count++;
                }

                if (error_count == 0)
                {
                    Console.WriteLine("Total records downloaded: " + output);
                    Console.WriteLine("May the SplunkFu be with you!");
                }
            }
        }
        
        static DateTime ParseDateTime(string input)
        {
            // ISO 8601: YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00) - TZD ignored
            int YYYY = Convert.ToInt32(input.Substring(0,4)); // four-digit year
            int MM = Convert.ToInt32(input.Substring(5, 2));   // two-digit month (01=January, etc.)
            int DD = Convert.ToInt32(input.Substring(8, 2));   // two-digit day of month (01 through 31)
            int hh = Convert.ToInt32(input.Substring(11, 2));   // two digits of hour (00 through 23) (am/pm NOT allowed)
            int mm = Convert.ToInt32(input.Substring(14, 2));   // two digits of minute (00 through 59)
            int ss = Convert.ToInt32(input.Substring(17, 2));   // two digits of second (00 through 59)
            int s = Convert.ToInt32(input.Substring(0, 2));    // one or more digits representing a decimal fraction of a second
            //string TZD  // time zone designator (Z or +hh:mm or -hh:mm)

            DateTime output = new DateTime(YYYY,MM,DD,hh,mm,ss);

            return output;
        }
        
        

        
    }
}
